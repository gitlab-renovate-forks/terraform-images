# Contributing

This project won't receive any Terraform specific updates,
due to the HashiCorp Terraform license change to BSL.

GitLab is no longer allowed to distribute Terraform.
